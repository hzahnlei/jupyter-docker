###############################################################################
#
# This Dockerfile dockerizes JupyterLab. It also adds support for Java bymeans
# of Spencer Park's IJava kernel. Being a Docker image allows this JupyterLab
# to be used locally or be deployed in the cloud.
#
# This Dockerfile is based on Spencer Park's Dockerfile to be found here:
# https://github.com/SpencerPark/ijava-binder
#
# Holger Zahnleiter, 2021-02-13
#
###############################################################################

FROM jupyter/base-notebook:lab-3.0.5

USER root

#---- Install basic dependencies ----------------------------------------------
RUN apt-get update
RUN apt-get install -y openjdk-11-jdk
RUN apt-get install -y maven
RUN apt-get install -y curl
RUN apt-get install -y unzip

#---- Install the IJava kernel ------------------------------------------------
RUN curl -L https://github.com/SpencerPark/IJava/releases/download/v1.3.0/ijava-1.3.0.zip > ijava-kernel.zip
RUN unzip ijava-kernel.zip -d ijava-kernel \
        && cd ijava-kernel \
        && python install.py --sys-prefix

#---- Prepare the environment and launch Jupyter ------------------------------
ENV NB_USER jovyan
ENV HOME /home/$NB_USER
USER $NB_USER
WORKDIR $HOME

CMD ["jupyter", "lab", "--ip", "0.0.0.0"]
